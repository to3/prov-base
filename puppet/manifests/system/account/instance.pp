# Define prov_base::system::account::instance
# Purpose: to add active local users

define prov_base::system::account::instance (
  $active_users,
  $realname = '',
  $sshkeys  = '',
  $password = '*',
  $shell    = '/bin/bash'
) {
  if member($active_users,$name) {
    prov_base::system::account::localuser { $name:
      realname => $realname,
      sshkeys => $sshkeys,
      password => $password,
      shell => $shell,
    }
  }
}

