#
#
#
class prov_base:::monitor::mysql (
  $monitor_user     = 'monitor',
  $monitor_password = 'monitor',
  $db_host	        = '127.0.0.1',
  $user_parameters_source = "puppet:///modules/${module_name}/profiles/zabbix/userparams/mysql.conf",
  $template         = 'Template App MySQL',
  $template_source  = undef,
) inherits prov_base:::monitoring::zabbix::common {

  prov_base:::monitor::service {'mysql':
    user_param_source => $user_parameters_source,
    template          => $template,
    template_source   => $template_source
  }

  file {'/etc/zabbix/.my.cnf':
    ensure => file,
    mode   => '0400',
    owner  => 'zabbix'
  }

  Ini_setting {
    path => '/etc/zabbix/.my.cnf',
  }

  ini_setting {'mysql_user':
    section => 'mysql',
    setting => 'user',
    value   => $monitor_user,
  }

  ini_setting {'mysql_password':
    section => 'mysql',
    setting => 'password',
    value   => $monitor_password,
  }

  ini_setting {'mysql_host':
    section => 'mysql',
    setting => 'host',
    value   => $db_host,
  }

  ini_setting {'mysqladmin_user':
    section => 'mysqladmin',
    setting => 'user',
    value   => $monitor_user,
  }

  ini_setting {'mysqladmin_password':
    section => 'mysqladmin',
    setting => 'password',
    value   => $monitor_password,
  }

  ini_setting {'mysqladmin_host':
    section => 'mysqladmin',
    setting => 'host',
    value   => $db_host,
  }

}
