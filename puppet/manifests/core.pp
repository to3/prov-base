#
# Class: prov_base::core
#

class prov_base::core (
  $monitoring_system = 'zabbix',
  $run_puppet_cron = false,
) {

  package {'run-one':
    ensure => present,
  }

  file { '/usr/local/bin/run_puppet.sh':
    ensure => file,
    mode   => '0755',
    source => "puppet:///modules/${module_name}/run_puppet.sh"
  }

  if $run_puppet_cron {
    cron { 'Puppetrun':
      command => 'run-one /usr/local/bin/run_puppet.sh 2>&1 | logger -t puppetrun',
      user    => 'root',
      require => [Package['run-one'], File['/usr/local/bin/run_puppet.sh']],
      minute  => '*/5',
    }
  }
  ##
  # zabbixapi gem is required for regular puppet run
  ##
  if $monitoring_system == 'zabbix' {
    contain prov_base::monitoring::zabbix::common
  }
}
