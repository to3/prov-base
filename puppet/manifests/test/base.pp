#
# Base class that sets up tests
#
class prov_base::test::base {

  package { 'nagios-plugins':
    ensure => present,
  }

  file { ['/usr/lib/prov', '/usr/lib/prov/tests']:
    ensure => directory,
  }

}
