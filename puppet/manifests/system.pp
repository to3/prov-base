## Class: prov_base::system
## Purpose: to group all system level configuration together.

class prov_base::system (
  $security_banner  = undef,
  $manage_timezone  = true,
  $manage_accounts  = true,
  $manage_ntp       = true,
  $manage_repo      = true,
  $enable_puppetlabs_repo = true,
  $hosts	    = {},
) {

  ##
  # Set timezone
  ##

  if $manage_timezone {
    contain ::timezone

    prov_base::test {'timezone.sh':}
  }

  ##
  # Setup package repos
  ##
  if $manage_repo {
    contain prov_base::system::repo
    if $enable_puppetlabs_repo {
      contain ::puppet::repo::puppetlabs
    }
  }
  ##
  # Setup ntp servers and ntp clients
  ##
  if $manage_ntp {
    contain ::ntp
    prov_base::test {'ntp.sh':}
  }

  ##
  # Setup user accounts
  ##

  if $manage_accounts {
    contain prov_base::system::account
  }

  ##
  # Added security banner messages
  ##

  $issue = [ '/etc/issue.net','/etc/issue' ]
  if $security_banner {
    file { $issue:
      ensure        => file,
      owner         => root,
      group         => root,
      mode          => 644,
      content       => $security_banner,
    }
  }

 create_resources('host',$hosts,{})

}
